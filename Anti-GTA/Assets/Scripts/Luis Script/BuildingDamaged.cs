﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingDamaged : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            Damage();
        }
    }

    /*private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Damage();
        }
    }*/

    private void Damage()
    {
        //Messenger.Broadcast(GameEvent.DAMAGED_PROPERTY);
        Constants.S.propertyDamaged += 10;
        Constants.S.paycheck -= 10;
        print("property has taken damage");
    }

}
