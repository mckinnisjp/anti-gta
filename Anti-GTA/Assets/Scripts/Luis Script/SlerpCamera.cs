﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlerpCamera : MonoBehaviour {

    public float slerpSpeed;

    public float smoothSpeed = 1.0f;
    public Vector3 offset;

    private Camera cam;
    private Transform camTransform;
    public Transform target;

    Quaternion lookAt;

    // Use this for initialization
    void Start () {
        cam = gameObject.GetComponent<Camera>();
        camTransform = cam.GetComponent<Transform>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void LateUpdate()
    {
        //TODO: get y angle, slerp just y angle

        lookAt = Quaternion.Slerp(camTransform.rotation, target.rotation, Time.fixedDeltaTime * slerpSpeed);

        camTransform.rotation = new Quaternion(camTransform.rotation.x, lookAt.y, camTransform.rotation.z, camTransform.rotation.w);

    }


}
