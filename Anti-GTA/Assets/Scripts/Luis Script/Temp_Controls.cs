﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Temp_Controls : MonoBehaviour {

    public float rotateSpeed = 0.5f;
    public float moveSpeed = 1.0f;
    public float weight = 100.0f;
    public float yLerp = 1.0f;

    public Transform backLeft;
    public Transform backRight;
    public Transform frontLeft;
    public Transform frontRight;
    public RaycastHit lr;
    public RaycastHit rr;
    public RaycastHit lf;
    public RaycastHit rf;
    public Vector3 upDir;

    private bool isGoingUp = false;
    private bool isGiongDown = false;
    

    SceneManager manage;

    private Vector3 moveDirection = Vector3.zero;
    private Vector3 road;
    private bool isGrounded = false;
    private float yPos;
    

    private CharacterController controller;
    private bool isFloored = false;

    private float speedBoost;

    public AudioSource rev;

    private float lastY;
    private bool hasRotated;

    // Use this for initialization
    void Start()
    {
        manage = FindObjectOfType<SceneManager>();
        controller = GetComponent<CharacterController>();
        speedBoost = 1.0f;
        lastY = transform.position.y;
        hasRotated = false;
        rev.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        
        Floored();
        if (isFloored)
        {
            lastY = transform.position.y;
            float forwardMotion = Input.GetAxis("Vertical");
            float angleMotion = Input.GetAxis("Horizontal");

            manage.setSpeedNeedle(forwardMotion);

            float curSpeed = speedBoost * moveSpeed * Input.GetAxis("Vertical");
            print(controller.isGrounded);

            transform.Rotate(0, Input.GetAxis("Horizontal") * rotateSpeed, 0);
            moveDirection = new Vector3(0, 0, Input.GetAxis("Vertical"));
            moveDirection = transform.TransformDirection(moveDirection);
            moveDirection *= moveSpeed * speedBoost;
            controller.Move(moveDirection * Time.deltaTime);

            float diff = Mathf.Abs(transform.position.y - lastY);
            if (transform.position.y != lastY && !hasRotated)
            {
                if (Mathf.Abs(transform.position.y) - Mathf.Abs(lastY) > 0.5f)
                {
                    transform.Rotate(new Vector3(10.0f, 0.0f));
                    hasRotated = true;
                }
                else if (Mathf.Abs(lastY) - Mathf.Abs(transform.position.y) > 0.5f)
                {
                    transform.Rotate(new Vector3(-10.0f, 0.0f));
                    hasRotated = true;
                }
                StartCoroutine(canRotate());
            }
            else if (transform.position.y == lastY)
            {
                transform.Rotate(new Vector3(0.0f, 0.0f));
            }
            

        }
       

        if (rev.time > 1)
        {
            rev.Stop();
        }

    }

  

    private void CheckGrounded()
    {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, -1 * transform.TransformDirection(Vector3.up), out hit))
        {
            
            road = hit.transform.position;
            isGrounded = true;
            print("Road.Y: " + hit);
        }
        
    }

    void Floored()
    {
        RaycastHit raycastResult;
        if (Physics.Raycast(transform.position, Vector3.down, out raycastResult, 1.0f))
        {
            
            transform.position = raycastResult.point + (raycastResult.normal.normalized * 0.5f);
            yPos = raycastResult.point.y;
        
            isFloored = true;
        }
        else
        {
            print("Not Floored");
            isFloored = false;
            transform.position = new Vector3(transform.position.x, yPos, transform.position.z);
        }
    }

    public void SetBoost(float b)
    {
        speedBoost = b;
    }

    IEnumerator canRotate()
    {
        hasRotated = true;
        yield return new WaitForSeconds(3.0f);
        hasRotated = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Property")
        {
            Damage();
        }
    }

    private void Damage()
    {
        //Messenger.Broadcast(GameEvent.DAMAGED_PROPERTY);
        Constants.S.propertyDamaged += 10;
        Constants.S.paycheck -= 10;
        print("property has taken damage");
    }
}
