﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DonutPowerUp : MonoBehaviour
{
    //public GameObject pickupEffect;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Pickup();
        }
    }

    private void Pickup()
    {
        //Instantiate(pickupEffect, transform.position, transform.rotation);

        //Messenger.Broadcast(GameEvent.PICKUP_DONUT);

        Constants.S.donutsCaught += 1;

        //Destroy(pickupEffect);
        Destroy(gameObject);
    }
}