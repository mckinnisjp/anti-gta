﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GasPowerUp : MonoBehaviour {
    public GameObject pickupEffect;
    private GameObject pickup;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Pickup();
        }
    }

    private void Pickup()
    {
        pickup = Instantiate(pickupEffect, transform.position, transform.rotation);
        Destroy(pickup, 2);

        Constants.S.gas += 0.2f;

        Destroy(gameObject);
    }
}
