﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AI_Car : MonoBehaviour {

    public GameObject donut;

    public SceneManager scene;

    public Transform[] targets;
    public float speed;
    public float turnSpeed;

    private Transform nextTarget;

    public Collider coll;

    private int index;
    private bool hasTarget;
    private bool hasTurned;

    private bool canCreateDonut;

    private Vector3 roadNormal;

	// Use this for initialization
	void Start () {
        scene = FindObjectOfType<SceneManager>();
        canCreateDonut = true;
        nextTarget = targets[0];
        index = 0;
        SetTarget(index);
        hasTarget = true;//May need to get rid of it
        hasTurned = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (Vector3.Distance(transform.position, nextTarget.position) < 3)
        {
            SetTarget(++index);
        }
        if (hasTurned)
        {
            transform.position = Vector3.MoveTowards(transform.position, nextTarget.position, Time.deltaTime * speed);
            
        }
        else
        {
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.LookRotation(nextTarget.transform.position - transform.position), Time.deltaTime * turnSpeed);
           
            if (transform.rotation.y - Quaternion.LookRotation(nextTarget.transform.position - transform.position).y < 0.01f)
            {
                hasTurned = true;
                //Vector3 newDir = Vector3.RotateTowards(transform.forward, nextTarget.position, Time.deltaTime, 0.5f);
                //transform.rotation = Quaternion.LookRotation(newDir);
                transform.rotation = Quaternion.LookRotation(nextTarget.transform.position - transform.position);
            }
            else
            {
                print("Can't turn");
                StartCoroutine(Stuck());
            }
            
        }
        if (canCreateDonut)
        {
            StartCoroutine(CreateDonut());
        }
        
	}

    private void SetTarget(int i)
    {
        if (i >= targets.Length)
        {
            //print("i is: " + i);
            print("Criminal escaped!");
            Constants.S.paycheck -= (2000 - Constants.S.finalPay);
            Constants.S.gasSpent = (int)(400 - Constants.S.gas * 400);
            UnityEngine.SceneManagement.SceneManager.LoadScene("GameOver");
            return;
        }
        nextTarget = targets[i];
        hasTurned = false;
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Road")
        {
            roadNormal = collision.gameObject.transform.up;

        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Constants.S.caughtCriminal = true;
        }
    }

    IEnumerator CreateDonut()
    {
        canCreateDonut = false;
        yield return new WaitForSeconds(5.0f);
        Instantiate(donut, transform.position, transform.rotation);
        canCreateDonut = true;
    }

    IEnumerator Stuck()
    {
        yield return new WaitForSeconds(Time.deltaTime * 3.0f);
        hasTurned = true;
    }
}
