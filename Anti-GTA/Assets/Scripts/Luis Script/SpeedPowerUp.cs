﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedPowerUp : MonoBehaviour {

    public GameObject pickupEffect;
    private GameObject effect;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            print("Hit by player");
            StartCoroutine(Pickup(other));
        }
    }

    IEnumerator Pickup(Collider player)
    {
        effect = Instantiate(pickupEffect, transform.position, transform.rotation);
        Destroy(effect, 2);


        //reference to player
        Temp_Controls control = player.GetComponent<Temp_Controls>();
        control.SetBoost(2.0f);
        control.rev.Play();

        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<Collider>().enabled = false;

        
        yield return new WaitForSeconds(1.0f);

        control.SetBoost(1.0f);
        //Destroy(pickupEffect);
        Destroy(gameObject);
    }
}
