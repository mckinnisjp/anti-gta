﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TakeDamage : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            Damage();
        }
    }

    private void Damage()
    {
        //Messenger.Broadcast(GameEvent.DAMAGED_PROPERTY);
        Constants.S.propertyDamaged += 100;
        Constants.S.paycheck -= 100;
        print("property has taken damage");
    }

}
