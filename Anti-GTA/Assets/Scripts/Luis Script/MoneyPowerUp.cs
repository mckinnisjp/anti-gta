﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoneyPowerUp : MonoBehaviour {
    public GameObject pickupEffect;
    private GameObject pickup;
    

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            Pickup();
        }
    }

    private void Pickup()
    {
        pickup = Instantiate(pickupEffect, transform.position, transform.rotation);
        Destroy(pickup, 2);

        //Messenger.Broadcast(GameEvent.PICKUP_MONEY);
        Constants.S.cashGrabbed += 100;
        Constants.S.paycheck += 100;
        Destroy(gameObject);
    }
}
