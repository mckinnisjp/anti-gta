﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CarController : MonoBehaviour {

    public WheelCollider front_right;
    public WheelCollider front_left;
    public float speed = 2.5f;

    private Rigidbody carRigidBody;

	// Use this for initialization
	void Start () {
        carRigidBody = gameObject.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    private void FixedUpdate()
    {
        float accel = Input.GetAxis("Horizontal");
        float steer = Input.GetAxis("Vertical");
        CarMove(accel, steer);
    }

    void CarMove(float steer, float accel)
    {
        //drifting when putting too much steer-ing input
        if (steer > 0.5f && steer < -0.5f)
        {
            front_left.steerAngle = steer * 10;
            front_right.steerAngle = steer * 10;
        }
        //Normal steer-ing
        else
        {
            front_left.steerAngle = steer * 5;
            front_right.steerAngle = steer * 5;
        }

        if (accel < 0)
        {
            front_right.motorTorque = 0;
            front_left.motorTorque = 0;
            carRigidBody.drag = 10;
        }
        else
        {
            front_right.motorTorque = accel * 3000.0f;
            front_left.motorTorque = accel * 3000.0f;
        }
        print("accel: " + accel);
        print("steer: " + steer);

        //carRigidBody.MovePosition(transform.position + transform.forward * accel * speed * Time.deltaTime);
        //carRigidBody.MoveRotation(new Quaternion(0, 1.0f, 0,0));//this.transform.Rotate(0, Input.GetAxis("Horizontal") * rotateSpeed, 0);
        //carRigidBody.MoveRotation(transform.rotation +  Time.deltaTime * steer);
        //carRigidBody.velocity *= 2.5f;
    }
}
