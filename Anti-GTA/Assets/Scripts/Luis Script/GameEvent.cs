﻿public static class GameEvent
{
    public const string PICKUP_MONEY = "PickupMoney";
    public const string PICKUP_DONUT = "PickupDonut";
    public const string DAMAGED_PROPERTY = "DamagedProperty";
}