﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SimpleTimer : MonoBehaviour {

    public float time;
    public Text timerText;

	// Use this for initialization
	void Start () {
        time = 0;
	}
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;
        if (time % 60 > 10)
        {
            timerText.text = "Time - " + (int)(time / 60) + ":" + (int)(time % 60);
        }
        else
        {
            timerText.text = "Time - " + (int)(time / 60) + ":0" + (int)(time % 60);
        }
	}
}
