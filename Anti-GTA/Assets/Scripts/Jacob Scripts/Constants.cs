﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants : MonoBehaviour {

    public static Constants S;

    public bool caughtCriminal;
    public int gasSpent;
    public int propertyDamaged;
    public int finalPay;
    public int donutsCaught;
    public int paycheck;
    public float gas;
    public int highScore;
    public int cashGrabbed;

    private void Awake()
    {
        if (S == null)
        {
            S = this;
        }
        caughtCriminal = false;
        gasSpent = 0;
        propertyDamaged = 0;
        finalPay = 2000;
        donutsCaught = 0;
        paycheck = 2000;
        gas = 1.0f;
        cashGrabbed = 0;
    }

        // Use this for initialization
        void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
