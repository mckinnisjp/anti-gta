﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoliceLightControl : MonoBehaviour {

    public GameObject redLight;
    public GameObject blueLight;
    float lightTime;

	// Use this for initialization
	void Start () {
        redLight.SetActive(true);
        blueLight.SetActive(false);
        lightTime = 0;
    }
	
	// Update is called once per frame
	void Update () {
		if (lightTime > 0.5f && lightTime < 3)
        {
            redLight.SetActive(false);
            blueLight.SetActive(true);
            lightTime = 3;
        }
        else if (lightTime > 3.5f)
        {
            redLight.SetActive(true);
            blueLight.SetActive(false);
            lightTime = 0;
        }
        lightTime += Time.deltaTime;
    }
}
