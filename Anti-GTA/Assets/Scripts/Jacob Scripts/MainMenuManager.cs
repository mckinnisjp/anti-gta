﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuManager : MonoBehaviour
{
    public AudioSource BackgroundMusic;

    public GameObject buttons;
    public GameObject controlsText;
    public GameObject creditsText;
    public GameObject backButton;

    // Use this for initialization
    void Start()
    {
        BackgroundMusic.Play();
        back();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void play()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Level 1");
    }

    public void controls()
    {
        buttons.SetActive(false);
        controlsText.SetActive(true);
        backButton.SetActive(true);
    }

    public void credits()
    {
        buttons.SetActive(false);
        creditsText.SetActive(true);
        backButton.SetActive(true);
    }

    public void back()
    {
        buttons.SetActive(true);
        controlsText.SetActive(false);
        creditsText.SetActive(false);
        backButton.SetActive(false);
    }
}
