﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverManage : MonoBehaviour {

    public Text passFailText;

    public Text infoText;

    string info;

    float time;

    private bool hasUpdated = false;



	// Use this for initialization
	void Start () {
        passFailText.supportRichText = true;
        infoText.supportRichText = true;
        info = "<color=lime>Original Paycheck: $2,000</color> \n";
        time = 0;
        
	}
	
	// Update is called once per frame
	void Update () {

        /**
         * need: 
         *      a boolean for catching or not the criminal -> bool caughtCriminal
         *      a float to keep track of gas money used
         *      a float to keep track of property damage
         *      a float to keep track of the final pay
         *      an int keeping track of donuts taken
         * 
         */

        // Adjust the subtitle to indicate success or failure
        if (Constants.S.caughtCriminal)
        {
            passFailText.text = "<color=lime>You Caught the Criminal!</color>";
        }
        else
        {
            passFailText.text = "<color=red>The Criminal Escaped</color>";
        }

        // Make the info text appear in sequence
        if (time > 5 && time < 10)
        {
            info += "<color=red>Gas Used: -$" + Constants.S.gasSpent + "</color> \n";
            time = 10;
        }
        else if (time > 15 && time < 20)
        {
            info += "<color=red>Property Damage: -$" + Constants.S.propertyDamaged + "</color> \n";
            time = 20;
        }
        else if (time > 25 && time < 30)
        {
            info += "<color=lime>Cash Grabbed: +$" + Constants.S.cashGrabbed + "</color> \n";
            time = 30;
        }
        else if (time > 35 && time < 40)
        {
            info += "<size=15><color=lime>Final Paycheck: $" + Constants.S.finalPay + "</color></size> \n ";
            time = 40;
        }
        else if (time > 45 && time < 50)
        {
            info += "<color=magenta>" + "Donuts Collected: " + Constants.S.donutsCaught + "</color> \n";
            time = 50;
        }

        if (!hasUpdated)
        {
            if (time > 55 && time < 60)
            {
                UpdateHighScore();
                hasUpdated = true;
                info += "<color=lime><size=15>High Score:</size> \t " + PlayerPrefs.GetInt("HighScore", 0).ToString() + "</color>";
            }
        }
            
        infoText.text = info;

        time += 4 * Time.deltaTime;
	}

    private void UpdateHighScore()
    {
        if (Constants.S.paycheck > PlayerPrefs.GetInt("HighScore", 0))
        {
            PlayerPrefs.SetInt("HighScore", Constants.S.paycheck);
           
        }
    }

    public void replay()
    {
        hasUpdated = false;
        UnityEngine.SceneManagement.SceneManager.LoadScene("Menu");
    }
}
