﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneManager : MonoBehaviour
{

    public GameObject needle;
    public GameObject gasNeedle;
    public Text payText;

    //public float gas;

    //public int paycheck;

    int needleRot;
    int gasNeedleRot;

    // Use this for initialization
    void Start()
    {
        needleRot = 228;
        gasNeedleRot = -145;
        Constants.S.gas = 1;
        //paycheck = 2000;
    }

    // Update is called once per frame
    void Update()
    {
        needle.transform.rotation = Quaternion.AngleAxis(needleRot, transform.forward);

        gasNeedle.transform.rotation = Quaternion.AngleAxis(gasNeedleRot, transform.forward);

        if (Constants.S.gas > 0)
        {
            gasNeedleRot = (int)(-145 * Constants.S.gas);
        }
        //gas -= 0.0001f;
        //Constants.S.gas -= Time.deltaTime / 100;

        //Constants.S.gas -= 0.0001f;

        //Pay Check Text Handling

        //Constants.S.gasSpent = (int)(400 - (Constants.S.gas * 400));
        //Constants.S.paycheck -= Constants.S.gasSpent;

        Constants.S.gas -= Time.deltaTime / 100;

        Constants.S.finalPay = Constants.S.paycheck - (int)(400 - Constants.S.gas * 400);
        //paycheck = Constants.S.paycheck - (int)(400 - gas * 400);

        //if (paycheck > 1800)
        if (Constants.S.finalPay > 1800)
        {
            payText.color = Color.blue;
            payText.fontSize = 25;
        }
        //else if (paycheck > 1700)
        else if (Constants.S.finalPay > 1700)
        {
            payText.color = Color.magenta;
            payText.fontSize = 30;
        }
        else
        {
            payText.color = Color.red;
            payText.fontSize = 35;
        }

        //payText.text = "Paycheck: $" + paycheck / 1000 + "," + paycheck % 1000;
        payText.text = "Paycheck: $" + Constants.S.finalPay / 1000 + "," + Constants.S.finalPay % 1000;

        if (Constants.S.finalPay < 1)
        {
            //Game Over
            Constants.S.caughtCriminal = false;
            Constants.S.paycheck -= (2000 - Constants.S.finalPay);
            Constants.S.gasSpent = (int)(400 - Constants.S.gas * 400);
            //Constants.S.gasSpent += (int)((1 - Constants.S.gas) * 400);
            //print(Constants.S.gasSpent);
            UnityEngine.SceneManagement.SceneManager.LoadScene("GameOver");
        }

        if (Constants.S.caughtCriminal)
        {
            //Constants.S.gasSpent += (int)((1 - Constants.S.gas) * 400);
            //print(Constants.S.gasSpent);
            Constants.S.paycheck -= (2000 - Constants.S.finalPay);
            Constants.S.gasSpent = (int)(400 - Constants.S.gas * 400);
            UnityEngine.SceneManagement.SceneManager.LoadScene("GameOver");
        }

    }

    public void setSpeedNeedle(float speed)
    {
        //Temporary needle code
        needleRot = 228 - (int)(speed * 276);
    }

    void Awake()
    {
        //What do we do if we can't replay the game?
        //Messenger.AddListener(GameEvent.PICKUP_MONEY, IncreasePayCheck);
        //Messenger.AddListener(GameEvent.PICKUP_DONUT, DonutsPowerUp);
        //Messenger.AddListener(GameEvent.DAMAGED_PROPERTY, Damaged);
    }

    void Damaged()
    {
        Constants.S.propertyDamaged += 10;
        Constants.S.paycheck -= 10;
    }

    void IncreasePayCheck()
    {
        Constants.S.gas += 0.2f;
        print("Paycheck increased");
    }

    void DonutsPowerUp()
    {
        Constants.S.donutsCaught += 1;
    }

}
